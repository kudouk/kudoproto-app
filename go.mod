module bitbucket.org/kudouk/kudoproto-app

go 1.12

require (
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/protobuf v1.3.1
	github.com/grpc-ecosystem/grpc-gateway v1.11.1
	github.com/mwitkow/go-proto-validators v0.0.0-20190212092829-1f388280e944
	google.golang.org/genproto v0.0.0-20190627203621-eb59cef1c072
	google.golang.org/grpc v1.20.1
)
