// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: user.d.proto

package proto

import (
	fmt "fmt"
	math "math"
	proto "github.com/golang/protobuf/proto"
	_ "github.com/mwitkow/go-proto-validators"
	github_com_mwitkow_go_proto_validators "github.com/mwitkow/go-proto-validators"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

func (this *AppUserAddress) Validate() error {
	return nil
}
func (this *AppUser) Validate() error {
	if this.Name == "" {
		return github_com_mwitkow_go_proto_validators.FieldError("Name", fmt.Errorf(`value '%v' must not be an empty string`, this.Name))
	}
	if this.Email == "" {
		return github_com_mwitkow_go_proto_validators.FieldError("Email", fmt.Errorf(`value '%v' must not be an empty string`, this.Email))
	}
	if this.Phone == "" {
		return github_com_mwitkow_go_proto_validators.FieldError("Phone", fmt.Errorf(`value '%v' must not be an empty string`, this.Phone))
	}
	for _, item := range this.Addresses {
		if item != nil {
			if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(item); err != nil {
				return github_com_mwitkow_go_proto_validators.FieldError("Addresses", err)
			}
		}
	}
	if this.ShippingAddress != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.ShippingAddress); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("ShippingAddress", err)
		}
	}
	if this.BillingAddress != nil {
		if err := github_com_mwitkow_go_proto_validators.CallValidatorIfExists(this.BillingAddress); err != nil {
			return github_com_mwitkow_go_proto_validators.FieldError("BillingAddress", err)
		}
	}
	return nil
}
