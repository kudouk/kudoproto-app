// Code generated by protoc-gen-go. DO NOT EDIT.
// source: user.app.api.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("user.app.api.proto", fileDescriptor_1f1a6fd24ecad752) }

var fileDescriptor_1f1a6fd24ecad752 = []byte{
	// 427 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x8c, 0x94, 0x3f, 0x8f, 0xd3, 0x30,
	0x18, 0xc6, 0x15, 0x24, 0xfe, 0xc8, 0x80, 0x90, 0x4c, 0x84, 0xda, 0x34, 0x02, 0x11, 0x40, 0xd0,
	0x20, 0x62, 0xd4, 0x6e, 0xdd, 0xda, 0x82, 0x10, 0x2b, 0x55, 0x17, 0x16, 0xe4, 0x36, 0x2f, 0xad,
	0x51, 0x1a, 0x9b, 0xd8, 0x61, 0x41, 0x2c, 0x4c, 0xac, 0xe8, 0xee, 0x2b, 0xdc, 0x70, 0x9f, 0xe6,
	0x86, 0xfb, 0x0a, 0xf7, 0x41, 0x4e, 0x71, 0xec, 0x5e, 0x53, 0xb9, 0x51, 0x27, 0x57, 0xef, 0xf3,
	0xf8, 0xfd, 0xfd, 0xea, 0x4a, 0x45, 0xb8, 0x94, 0x50, 0x24, 0x54, 0x88, 0x84, 0x0a, 0x96, 0x88,
	0x82, 0x2b, 0x8e, 0x6f, 0xeb, 0x23, 0x78, 0xa0, 0xa3, 0x4d, 0x3d, 0x0c, 0xc2, 0x15, 0xe7, 0xab,
	0x0c, 0x08, 0x15, 0x8c, 0xd0, 0x3c, 0xe7, 0x8a, 0x2a, 0xc6, 0x73, 0x59, 0xa7, 0x83, 0x8b, 0x7b,
	0xe8, 0xd1, 0xb4, 0x94, 0x8a, 0x6f, 0xa0, 0x98, 0x41, 0xf1, 0x8b, 0x2d, 0x01, 0xff, 0x40, 0x0f,
	0xa7, 0x05, 0x50, 0x05, 0x63, 0x21, 0xe6, 0x12, 0x0a, 0xdc, 0xab, 0xcb, 0x49, 0x63, 0xfa, 0x05,
	0x7e, 0x96, 0x20, 0x55, 0x10, 0xba, 0x43, 0x29, 0x78, 0x2e, 0x21, 0x7a, 0xfa, 0xf7, 0xf2, 0xea,
	0xe4, 0x56, 0x27, 0x7a, 0x4c, 0xa8, 0x10, 0xda, 0x41, 0x90, 0xa5, 0xe1, 0x8d, 0xbc, 0x18, 0x03,
	0xba, 0xff, 0x31, 0x65, 0xca, 0x92, 0xba, 0x66, 0xd9, 0xce, 0xcc, 0x72, 0x02, 0x57, 0x64, 0x28,
	0xcf, 0x34, 0xa5, 0x3b, 0xf0, 0xb7, 0x14, 0xcb, 0x20, 0x1b, 0xa8, 0x30, 0xdf, 0x10, 0xfa, 0x04,
	0x5b, 0x4a, 0xc7, 0xac, 0xba, 0x19, 0x59, 0x48, 0xd7, 0x91, 0x18, 0x46, 0xa8, 0x19, 0x4f, 0xb0,
	0x93, 0x81, 0xff, 0x79, 0xc8, 0x6f, 0xbc, 0xc0, 0x38, 0x4d, 0x0b, 0x90, 0x12, 0x47, 0xae, 0xe7,
	0x31, 0xa1, 0xa5, 0xbe, 0x68, 0xed, 0x18, 0xfe, 0x6b, 0xcd, 0x7f, 0x1e, 0x85, 0x2e, 0x3e, 0xa1,
	0x75, 0xbb, 0xfa, 0xae, 0xa7, 0x1e, 0xf2, 0xe7, 0x22, 0x3d, 0xac, 0xe2, 0x0a, 0xf7, 0x55, 0xdc,
	0x1d, 0xa3, 0x32, 0xd4, 0x2a, 0xef, 0x06, 0x6f, 0xda, 0x54, 0xc8, 0x6f, 0xf3, 0xe1, 0x73, 0xfa,
	0xa7, 0xd2, 0xfa, 0xef, 0x21, 0xff, 0x03, 0x64, 0x70, 0x50, 0xcb, 0x15, 0xee, 0x6b, 0xb9, 0x3b,
	0x46, 0xeb, 0xbd, 0xd6, 0x8a, 0xe3, 0xa3, 0xb5, 0xf0, 0xb9, 0x87, 0xc2, 0xe9, 0x9a, 0x73, 0x69,
	0x57, 0xce, 0xd6, 0x4c, 0x08, 0x96, 0xaf, 0xac, 0x5b, 0x6c, 0x7f, 0x99, 0x96, 0x92, 0x75, 0x7c,
	0x7b, 0x54, 0xb7, 0xe9, 0x1a, 0xbd, 0x72, 0xba, 0xa6, 0xf0, 0x9d, 0x96, 0x99, 0x22, 0xd2, 0xdc,
	0xae, 0xde, 0xef, 0xcc, 0x43, 0xbd, 0xc6, 0xea, 0x09, 0xcb, 0xb2, 0x1d, 0xd5, 0xbe, 0x0b, 0xdf,
	0xec, 0x58, 0xd3, 0xf8, 0x98, 0xaa, 0x11, 0x25, 0x5a, 0xb4, 0x1f, 0xbd, 0x6c, 0x15, 0x5d, 0xd4,
	0x97, 0x47, 0x5e, 0x3c, 0xb9, 0xfb, 0xb5, 0xfe, 0x1b, 0x5a, 0xdc, 0xd1, 0xc7, 0xf0, 0x3a, 0x00,
	0x00, 0xff, 0xff, 0xaf, 0xa6, 0x6c, 0xda, 0xaa, 0x04, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// CustomerServiceClient is the client API for CustomerService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type CustomerServiceClient interface {
	CreateAppUser(ctx context.Context, in *CreateAppUserRequest, opts ...grpc.CallOption) (*CreateAppUserResponse, error)
	EditAppUser(ctx context.Context, in *EditAppUserRequest, opts ...grpc.CallOption) (*EditAppUserResponse, error)
	GetAppUser(ctx context.Context, in *GetAppUserRequest, opts ...grpc.CallOption) (*GetAppUserResponse, error)
	CreateAppUserAddress(ctx context.Context, in *CreateAppUserAddressRequest, opts ...grpc.CallOption) (*CreateAppUserAddressResponse, error)
	UpdateAppUserAddress(ctx context.Context, in *UpdateAppUserAddressRequest, opts ...grpc.CallOption) (*UpdateAppUserAddressResponse, error)
	DeleteAppUserAddress(ctx context.Context, in *DeleteAppUserAddressRequest, opts ...grpc.CallOption) (*DeleteAppUserAddressResponse, error)
	ChooseAppUserShippingAddress(ctx context.Context, in *ChooseAppUserShippingAddressRequest, opts ...grpc.CallOption) (*ChooseAppUserShippingAddressResponse, error)
	ChooseAppUserBillingAddress(ctx context.Context, in *ChooseAppUserBillingAddressRequest, opts ...grpc.CallOption) (*ChooseAppUserBillingAddressResponse, error)
}

type customerServiceClient struct {
	cc *grpc.ClientConn
}

func NewCustomerServiceClient(cc *grpc.ClientConn) CustomerServiceClient {
	return &customerServiceClient{cc}
}

func (c *customerServiceClient) CreateAppUser(ctx context.Context, in *CreateAppUserRequest, opts ...grpc.CallOption) (*CreateAppUserResponse, error) {
	out := new(CreateAppUserResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/CreateAppUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) EditAppUser(ctx context.Context, in *EditAppUserRequest, opts ...grpc.CallOption) (*EditAppUserResponse, error) {
	out := new(EditAppUserResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/EditAppUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) GetAppUser(ctx context.Context, in *GetAppUserRequest, opts ...grpc.CallOption) (*GetAppUserResponse, error) {
	out := new(GetAppUserResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/GetAppUser", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) CreateAppUserAddress(ctx context.Context, in *CreateAppUserAddressRequest, opts ...grpc.CallOption) (*CreateAppUserAddressResponse, error) {
	out := new(CreateAppUserAddressResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/CreateAppUserAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) UpdateAppUserAddress(ctx context.Context, in *UpdateAppUserAddressRequest, opts ...grpc.CallOption) (*UpdateAppUserAddressResponse, error) {
	out := new(UpdateAppUserAddressResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/UpdateAppUserAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) DeleteAppUserAddress(ctx context.Context, in *DeleteAppUserAddressRequest, opts ...grpc.CallOption) (*DeleteAppUserAddressResponse, error) {
	out := new(DeleteAppUserAddressResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/DeleteAppUserAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) ChooseAppUserShippingAddress(ctx context.Context, in *ChooseAppUserShippingAddressRequest, opts ...grpc.CallOption) (*ChooseAppUserShippingAddressResponse, error) {
	out := new(ChooseAppUserShippingAddressResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/ChooseAppUserShippingAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *customerServiceClient) ChooseAppUserBillingAddress(ctx context.Context, in *ChooseAppUserBillingAddressRequest, opts ...grpc.CallOption) (*ChooseAppUserBillingAddressResponse, error) {
	out := new(ChooseAppUserBillingAddressResponse)
	err := c.cc.Invoke(ctx, "/proto.CustomerService/ChooseAppUserBillingAddress", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// CustomerServiceServer is the server API for CustomerService service.
type CustomerServiceServer interface {
	CreateAppUser(context.Context, *CreateAppUserRequest) (*CreateAppUserResponse, error)
	EditAppUser(context.Context, *EditAppUserRequest) (*EditAppUserResponse, error)
	GetAppUser(context.Context, *GetAppUserRequest) (*GetAppUserResponse, error)
	CreateAppUserAddress(context.Context, *CreateAppUserAddressRequest) (*CreateAppUserAddressResponse, error)
	UpdateAppUserAddress(context.Context, *UpdateAppUserAddressRequest) (*UpdateAppUserAddressResponse, error)
	DeleteAppUserAddress(context.Context, *DeleteAppUserAddressRequest) (*DeleteAppUserAddressResponse, error)
	ChooseAppUserShippingAddress(context.Context, *ChooseAppUserShippingAddressRequest) (*ChooseAppUserShippingAddressResponse, error)
	ChooseAppUserBillingAddress(context.Context, *ChooseAppUserBillingAddressRequest) (*ChooseAppUserBillingAddressResponse, error)
}

// UnimplementedCustomerServiceServer can be embedded to have forward compatible implementations.
type UnimplementedCustomerServiceServer struct {
}

func (*UnimplementedCustomerServiceServer) CreateAppUser(ctx context.Context, req *CreateAppUserRequest) (*CreateAppUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAppUser not implemented")
}
func (*UnimplementedCustomerServiceServer) EditAppUser(ctx context.Context, req *EditAppUserRequest) (*EditAppUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method EditAppUser not implemented")
}
func (*UnimplementedCustomerServiceServer) GetAppUser(ctx context.Context, req *GetAppUserRequest) (*GetAppUserResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAppUser not implemented")
}
func (*UnimplementedCustomerServiceServer) CreateAppUserAddress(ctx context.Context, req *CreateAppUserAddressRequest) (*CreateAppUserAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateAppUserAddress not implemented")
}
func (*UnimplementedCustomerServiceServer) UpdateAppUserAddress(ctx context.Context, req *UpdateAppUserAddressRequest) (*UpdateAppUserAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateAppUserAddress not implemented")
}
func (*UnimplementedCustomerServiceServer) DeleteAppUserAddress(ctx context.Context, req *DeleteAppUserAddressRequest) (*DeleteAppUserAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteAppUserAddress not implemented")
}
func (*UnimplementedCustomerServiceServer) ChooseAppUserShippingAddress(ctx context.Context, req *ChooseAppUserShippingAddressRequest) (*ChooseAppUserShippingAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ChooseAppUserShippingAddress not implemented")
}
func (*UnimplementedCustomerServiceServer) ChooseAppUserBillingAddress(ctx context.Context, req *ChooseAppUserBillingAddressRequest) (*ChooseAppUserBillingAddressResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ChooseAppUserBillingAddress not implemented")
}

func RegisterCustomerServiceServer(s *grpc.Server, srv CustomerServiceServer) {
	s.RegisterService(&_CustomerService_serviceDesc, srv)
}

func _CustomerService_CreateAppUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateAppUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).CreateAppUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/CreateAppUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).CreateAppUser(ctx, req.(*CreateAppUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_EditAppUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EditAppUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).EditAppUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/EditAppUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).EditAppUser(ctx, req.(*EditAppUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_GetAppUser_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAppUserRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).GetAppUser(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/GetAppUser",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).GetAppUser(ctx, req.(*GetAppUserRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_CreateAppUserAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateAppUserAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).CreateAppUserAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/CreateAppUserAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).CreateAppUserAddress(ctx, req.(*CreateAppUserAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_UpdateAppUserAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateAppUserAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).UpdateAppUserAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/UpdateAppUserAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).UpdateAppUserAddress(ctx, req.(*UpdateAppUserAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_DeleteAppUserAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteAppUserAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).DeleteAppUserAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/DeleteAppUserAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).DeleteAppUserAddress(ctx, req.(*DeleteAppUserAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_ChooseAppUserShippingAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ChooseAppUserShippingAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).ChooseAppUserShippingAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/ChooseAppUserShippingAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).ChooseAppUserShippingAddress(ctx, req.(*ChooseAppUserShippingAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _CustomerService_ChooseAppUserBillingAddress_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ChooseAppUserBillingAddressRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(CustomerServiceServer).ChooseAppUserBillingAddress(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.CustomerService/ChooseAppUserBillingAddress",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(CustomerServiceServer).ChooseAppUserBillingAddress(ctx, req.(*ChooseAppUserBillingAddressRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _CustomerService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.CustomerService",
	HandlerType: (*CustomerServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateAppUser",
			Handler:    _CustomerService_CreateAppUser_Handler,
		},
		{
			MethodName: "EditAppUser",
			Handler:    _CustomerService_EditAppUser_Handler,
		},
		{
			MethodName: "GetAppUser",
			Handler:    _CustomerService_GetAppUser_Handler,
		},
		{
			MethodName: "CreateAppUserAddress",
			Handler:    _CustomerService_CreateAppUserAddress_Handler,
		},
		{
			MethodName: "UpdateAppUserAddress",
			Handler:    _CustomerService_UpdateAppUserAddress_Handler,
		},
		{
			MethodName: "DeleteAppUserAddress",
			Handler:    _CustomerService_DeleteAppUserAddress_Handler,
		},
		{
			MethodName: "ChooseAppUserShippingAddress",
			Handler:    _CustomerService_ChooseAppUserShippingAddress_Handler,
		},
		{
			MethodName: "ChooseAppUserBillingAddress",
			Handler:    _CustomerService_ChooseAppUserBillingAddress_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "user.app.api.proto",
}
