VERSION = "unset"
DATE=$(shell date -u +%Y-%m-%d-%H:%M:%S-%Z)
DOCKER_IMAGE ?=	kudo/admin-api-gateway
IMAGE_TAG ?= latest

codegen:
	cp ../../services/kudo-service/proto/product/*.d.proto ./
	cp ../../services/kudo-service/proto/attribute/*.d.proto ./
	cp ../../services/kudo-service/proto/product/*.app.api.proto ./
	cp ../../services/specification-service/proto/**/*.m.proto ./
	cp ../../services/specification-service/proto/**/*.app.api.proto ./
	cp ../../services/cart-service/proto/**/*.m.proto ./
	cp ../../services/cart-service/proto/**/*.app.api.proto ./
	cp ../../services/customer-service/proto/**/*.m.proto ./
	cp ../../services/customer-service/proto/**/*.d.proto ./
	cp ../../services/customer-service/proto/**/*.app.api.proto ./
	cp ../../services/order-service/proto/**/*.m.proto ./
	cp ../../services/order-service/proto/**/*.app.api.proto ./
	cp ../../services/media-service/proto/**/*.m.proto ./
	cp ../../services/media-service/proto/**/*.app.api.proto ./
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		-I${GOPATH}/pkg/mod \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		facet.d.proto image.d.proto \
		attribute.d.proto collection.d.proto product.d.proto product.app.api.proto \
		cart.m.proto cart.app.api.proto \
		specification.m.proto task.m.proto tender.m.proto specification.app.api.proto \
		user.d.proto user.m.proto user.app.api.proto \
		order.m.proto order.app.api.proto \
		media.m.proto media.app.api.proto

kudo:
	cp ../../kudo-service/proto/product/*.d.proto ./
	cp ../../kudo-service/proto/attribute/*.d.proto ./
	cp ../../kudo-service/proto/product/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
	collection.d.proto image.d.proto attribute.d.proto facet.d.proto product.d.proto product.app.api.proto

specification:
	cp ../../kudo-specification/proto/specification/*.m.proto ./
	cp ../../kudo-specification/proto/specification/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
	specification.m.proto tender.m.proto specification.app.api.proto

cart:
	cp ../../kudo-cart/proto/cart/*.m.proto ./
	cp ../../kudo-cart/proto/cart/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
	image.d.proto attribute.d.proto media.m.proto cart.m.proto cart.app.api.proto

customer:
	cp ../../kudo-customer/proto/**/*.m.proto ./
	cp ../../kudo-customer/proto/**/*.d.proto ./
	cp ../../kudo-customer/proto/**/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		user.d.proto user.m.proto user.app.api.proto

order:
	cp ../../kudo-orders/proto/**/*.m.proto ./
	cp ../../kudo-orders/proto/**/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		image.d.proto attribute.d.proto cart.m.proto media.m.proto order.m.proto order.app.api.proto

media:
	cp ../../kudo-media/proto/**/*.m.proto ./
	cp ../../kudo-media/proto/**/*.app.api.proto ./
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
	GO111MODULE=off go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-swagger
	go install $(shell go list -f '{{ .Dir }}' -m github.com/golang/protobuf)/protoc-gen-go
	protoc -I/usr/local/include -I. \
		-I${GOPATH}/src \
		-I${GOPATH}/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis \
		--grpc-gateway_out=logtostderr=true:. \
		--swagger_out=logtostderr=true:. \
		--go_out=plugins=grpc:. \
		--govalidators_out=. \
		media.m.proto media.app.api.proto