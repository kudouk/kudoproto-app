// Code generated by protoc-gen-go. DO NOT EDIT.
// source: attribute.app.api.proto

package proto

import (
	context "context"
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

func init() { proto.RegisterFile("attribute.app.api.proto", fileDescriptor_5c26301fd9f6ec36) }

var fileDescriptor_5c26301fd9f6ec36 = []byte{
	// 181 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0x12, 0x4f, 0x2c, 0x29, 0x29,
	0xca, 0x4c, 0x2a, 0x2d, 0x49, 0xd5, 0x4b, 0x2c, 0x28, 0xd0, 0x4b, 0x2c, 0xc8, 0xd4, 0x2b, 0x28,
	0xca, 0x2f, 0xc9, 0x17, 0x62, 0x05, 0x53, 0x52, 0x82, 0x08, 0xf9, 0x14, 0x88, 0x8c, 0x94, 0x4c,
	0x7a, 0x7e, 0x7e, 0x7a, 0x4e, 0xaa, 0x7e, 0x62, 0x41, 0xa6, 0x7e, 0x62, 0x5e, 0x5e, 0x7e, 0x49,
	0x62, 0x49, 0x66, 0x7e, 0x5e, 0x31, 0x44, 0xd6, 0x68, 0x1a, 0x23, 0x97, 0x80, 0x23, 0x4c, 0x4f,
	0x70, 0x6a, 0x51, 0x59, 0x66, 0x72, 0xaa, 0x50, 0x23, 0x23, 0x97, 0x90, 0x7b, 0x6a, 0x09, 0x5c,
	0xbc, 0xd8, 0xa9, 0xd2, 0x33, 0xa5, 0x58, 0x48, 0x01, 0xa2, 0x47, 0x0f, 0x53, 0x2a, 0x28, 0xb5,
	0xb0, 0x34, 0xb5, 0xb8, 0x44, 0x4a, 0x11, 0x8f, 0x8a, 0xe2, 0x82, 0xfc, 0xbc, 0xe2, 0x54, 0x25,
	0xb5, 0xa6, 0xcb, 0x4f, 0x26, 0x33, 0x29, 0x08, 0xc9, 0xe9, 0x27, 0x16, 0x14, 0x80, 0x1d, 0x55,
	0xa0, 0x0f, 0x77, 0xb4, 0x7e, 0x66, 0x4a, 0xb1, 0x7e, 0x75, 0x66, 0x4a, 0x71, 0xad, 0x13, 0x7b,
	0x14, 0xc4, 0x4b, 0x49, 0x6c, 0x60, 0xca, 0x18, 0x10, 0x00, 0x00, 0xff, 0xff, 0x67, 0x6f, 0x0d,
	0xfd, 0xfb, 0x00, 0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// AttributeServiceClient is the client API for AttributeService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type AttributeServiceClient interface {
	GetAttributesByIds(ctx context.Context, in *GetAttributesByIdsRequest, opts ...grpc.CallOption) (*GetAttributesByIdsResponse, error)
}

type attributeServiceClient struct {
	cc *grpc.ClientConn
}

func NewAttributeServiceClient(cc *grpc.ClientConn) AttributeServiceClient {
	return &attributeServiceClient{cc}
}

func (c *attributeServiceClient) GetAttributesByIds(ctx context.Context, in *GetAttributesByIdsRequest, opts ...grpc.CallOption) (*GetAttributesByIdsResponse, error) {
	out := new(GetAttributesByIdsResponse)
	err := c.cc.Invoke(ctx, "/proto.AttributeService/GetAttributesByIds", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// AttributeServiceServer is the server API for AttributeService service.
type AttributeServiceServer interface {
	GetAttributesByIds(context.Context, *GetAttributesByIdsRequest) (*GetAttributesByIdsResponse, error)
}

// UnimplementedAttributeServiceServer can be embedded to have forward compatible implementations.
type UnimplementedAttributeServiceServer struct {
}

func (*UnimplementedAttributeServiceServer) GetAttributesByIds(ctx context.Context, req *GetAttributesByIdsRequest) (*GetAttributesByIdsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetAttributesByIds not implemented")
}

func RegisterAttributeServiceServer(s *grpc.Server, srv AttributeServiceServer) {
	s.RegisterService(&_AttributeService_serviceDesc, srv)
}

func _AttributeService_GetAttributesByIds_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetAttributesByIdsRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(AttributeServiceServer).GetAttributesByIds(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/proto.AttributeService/GetAttributesByIds",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(AttributeServiceServer).GetAttributesByIds(ctx, req.(*GetAttributesByIdsRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _AttributeService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "proto.AttributeService",
	HandlerType: (*AttributeServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetAttributesByIds",
			Handler:    _AttributeService_GetAttributesByIds_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "attribute.app.api.proto",
}
