// Code generated by protoc-gen-go. DO NOT EDIT.
// source: supplierTemp.d.proto

package proto

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "github.com/mwitkow/go-proto-validators"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type CompanyTemp struct {
	Id                   int64    `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	CreatedAt            string   `protobuf:"bytes,2,opt,name=createdAt,proto3" json:"createdAt,omitempty"`
	CompanyName          string   `protobuf:"bytes,3,opt,name=companyName,proto3" json:"companyName,omitempty"`
	CompanyNumber        string   `protobuf:"bytes,4,opt,name=companyNumber,proto3" json:"companyNumber,omitempty"`
	CompanyWebsite       string   `protobuf:"bytes,5,opt,name=companyWebsite,proto3" json:"companyWebsite,omitempty"`
	CompanyPhone         string   `protobuf:"bytes,6,opt,name=companyPhone,proto3" json:"companyPhone,omitempty"`
	CompanyEmail         string   `protobuf:"bytes,7,opt,name=companyEmail,proto3" json:"companyEmail,omitempty"`
	ContactEmail         string   `protobuf:"bytes,8,opt,name=contactEmail,proto3" json:"contactEmail,omitempty"`
	ContactName          string   `protobuf:"bytes,9,opt,name=contactName,proto3" json:"contactName,omitempty"`
	ContactPhone         string   `protobuf:"bytes,10,opt,name=contactPhone,proto3" json:"contactPhone,omitempty"`
	AddressLine1         string   `protobuf:"bytes,11,opt,name=addressLine1,proto3" json:"addressLine1,omitempty"`
	AddressLine2         string   `protobuf:"bytes,12,opt,name=addressLine2,proto3" json:"addressLine2,omitempty"`
	AddressCity          string   `protobuf:"bytes,13,opt,name=addressCity,proto3" json:"addressCity,omitempty"`
	AddressZip           string   `protobuf:"bytes,14,opt,name=addressZip,proto3" json:"addressZip,omitempty"`
	AddressCountry       string   `protobuf:"bytes,15,opt,name=addressCountry,proto3" json:"addressCountry,omitempty"`
	WhiteLabel           bool     `protobuf:"varint,16,opt,name=whiteLabel,proto3" json:"whiteLabel,omitempty"`
	PrintingMethods      []int64  `protobuf:"varint,17,rep,packed,name=printingMethods,proto3" json:"printingMethods,omitempty"`
	FinishingOptions     []int64  `protobuf:"varint,18,rep,packed,name=finishingOptions,proto3" json:"finishingOptions,omitempty"`
	ProductCategories    []int64  `protobuf:"varint,19,rep,packed,name=productCategories,proto3" json:"productCategories,omitempty"`
	ApplicationNotes     string   `protobuf:"bytes,20,opt,name=applicationNotes,proto3" json:"applicationNotes,omitempty"`
	Verified             bool     `protobuf:"varint,22,opt,name=verified,proto3" json:"verified,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CompanyTemp) Reset()         { *m = CompanyTemp{} }
func (m *CompanyTemp) String() string { return proto.CompactTextString(m) }
func (*CompanyTemp) ProtoMessage()    {}
func (*CompanyTemp) Descriptor() ([]byte, []int) {
	return fileDescriptor_bae84fc5fd5e771d, []int{0}
}

func (m *CompanyTemp) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CompanyTemp.Unmarshal(m, b)
}
func (m *CompanyTemp) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CompanyTemp.Marshal(b, m, deterministic)
}
func (m *CompanyTemp) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CompanyTemp.Merge(m, src)
}
func (m *CompanyTemp) XXX_Size() int {
	return xxx_messageInfo_CompanyTemp.Size(m)
}
func (m *CompanyTemp) XXX_DiscardUnknown() {
	xxx_messageInfo_CompanyTemp.DiscardUnknown(m)
}

var xxx_messageInfo_CompanyTemp proto.InternalMessageInfo

func (m *CompanyTemp) GetId() int64 {
	if m != nil {
		return m.Id
	}
	return 0
}

func (m *CompanyTemp) GetCreatedAt() string {
	if m != nil {
		return m.CreatedAt
	}
	return ""
}

func (m *CompanyTemp) GetCompanyName() string {
	if m != nil {
		return m.CompanyName
	}
	return ""
}

func (m *CompanyTemp) GetCompanyNumber() string {
	if m != nil {
		return m.CompanyNumber
	}
	return ""
}

func (m *CompanyTemp) GetCompanyWebsite() string {
	if m != nil {
		return m.CompanyWebsite
	}
	return ""
}

func (m *CompanyTemp) GetCompanyPhone() string {
	if m != nil {
		return m.CompanyPhone
	}
	return ""
}

func (m *CompanyTemp) GetCompanyEmail() string {
	if m != nil {
		return m.CompanyEmail
	}
	return ""
}

func (m *CompanyTemp) GetContactEmail() string {
	if m != nil {
		return m.ContactEmail
	}
	return ""
}

func (m *CompanyTemp) GetContactName() string {
	if m != nil {
		return m.ContactName
	}
	return ""
}

func (m *CompanyTemp) GetContactPhone() string {
	if m != nil {
		return m.ContactPhone
	}
	return ""
}

func (m *CompanyTemp) GetAddressLine1() string {
	if m != nil {
		return m.AddressLine1
	}
	return ""
}

func (m *CompanyTemp) GetAddressLine2() string {
	if m != nil {
		return m.AddressLine2
	}
	return ""
}

func (m *CompanyTemp) GetAddressCity() string {
	if m != nil {
		return m.AddressCity
	}
	return ""
}

func (m *CompanyTemp) GetAddressZip() string {
	if m != nil {
		return m.AddressZip
	}
	return ""
}

func (m *CompanyTemp) GetAddressCountry() string {
	if m != nil {
		return m.AddressCountry
	}
	return ""
}

func (m *CompanyTemp) GetWhiteLabel() bool {
	if m != nil {
		return m.WhiteLabel
	}
	return false
}

func (m *CompanyTemp) GetPrintingMethods() []int64 {
	if m != nil {
		return m.PrintingMethods
	}
	return nil
}

func (m *CompanyTemp) GetFinishingOptions() []int64 {
	if m != nil {
		return m.FinishingOptions
	}
	return nil
}

func (m *CompanyTemp) GetProductCategories() []int64 {
	if m != nil {
		return m.ProductCategories
	}
	return nil
}

func (m *CompanyTemp) GetApplicationNotes() string {
	if m != nil {
		return m.ApplicationNotes
	}
	return ""
}

func (m *CompanyTemp) GetVerified() bool {
	if m != nil {
		return m.Verified
	}
	return false
}

func init() {
	proto.RegisterType((*CompanyTemp)(nil), "proto.CompanyTemp")
}

func init() { proto.RegisterFile("supplierTemp.d.proto", fileDescriptor_bae84fc5fd5e771d) }

var fileDescriptor_bae84fc5fd5e771d = []byte{
	// 455 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0x6c, 0x93, 0xdf, 0x6a, 0xdb, 0x30,
	0x14, 0x87, 0x71, 0xb2, 0xa6, 0x89, 0x92, 0xa6, 0xad, 0x56, 0x86, 0x28, 0x63, 0x33, 0xbd, 0x18,
	0xa6, 0xb4, 0x09, 0xeb, 0x60, 0xf7, 0x6b, 0xd8, 0x5d, 0xd7, 0x0d, 0x33, 0xd8, 0xe8, 0x9d, 0x6c,
	0x9d, 0xda, 0x87, 0xc5, 0x92, 0x91, 0xe4, 0x86, 0x3c, 0xd7, 0x1e, 0x68, 0xb0, 0x27, 0x29, 0x96,
	0xd2, 0xd4, 0x76, 0x7a, 0x25, 0x9f, 0xef, 0xf7, 0xe9, 0x58, 0x7f, 0x10, 0x39, 0x31, 0x55, 0x59,
	0x2e, 0x11, 0xf4, 0x4f, 0x28, 0xca, 0x99, 0x98, 0x95, 0x5a, 0x59, 0x45, 0xf7, 0xdc, 0x70, 0xfa,
	0x39, 0x43, 0x9b, 0x57, 0xc9, 0x2c, 0x55, 0xc5, 0xbc, 0x58, 0xa1, 0xfd, 0xa3, 0x56, 0xf3, 0x4c,
	0x5d, 0xba, 0xf0, 0xf2, 0x81, 0x2f, 0x51, 0x70, 0xab, 0xb4, 0x99, 0x6f, 0x3f, 0xfd, 0xf4, 0xb3,
	0xbf, 0x03, 0x32, 0x5e, 0xa8, 0xa2, 0xe4, 0x72, 0x5d, 0xb7, 0xa5, 0x53, 0xd2, 0x43, 0xc1, 0x82,
	0x30, 0x88, 0xfa, 0x71, 0x0f, 0x05, 0x7d, 0x4b, 0x46, 0xa9, 0x06, 0x6e, 0x41, 0x7c, 0xb1, 0xac,
	0x17, 0x06, 0xd1, 0x28, 0x7e, 0x06, 0x34, 0x22, 0xe3, 0xd4, 0x4f, 0xbe, 0xe5, 0x05, 0xb0, 0x7e,
	0x9d, 0x5f, 0x0f, 0xfe, 0xff, 0x7b, 0xdf, 0xfb, 0x1d, 0xc4, 0xcd, 0x88, 0x5e, 0x90, 0x83, 0xa7,
	0xb2, 0x2a, 0x12, 0xd0, 0xec, 0x55, 0xcb, 0x6d, 0x87, 0xf4, 0x03, 0x99, 0x6e, 0xc0, 0x2f, 0x48,
	0x0c, 0x5a, 0x60, 0x7b, 0xee, 0xd7, 0x1d, 0x4a, 0xcf, 0xc9, 0x64, 0x43, 0x7e, 0xe4, 0x4a, 0x02,
	0x1b, 0xb4, 0x9a, 0xb6, 0xb2, 0x86, 0xfb, 0xb5, 0xe0, 0xb8, 0x64, 0xfb, 0x2f, 0xba, 0x2e, 0xf3,
	0xae, 0xb4, 0x3c, 0xb5, 0xde, 0x1d, 0x76, 0xdd, 0xe7, 0xcc, 0x9f, 0x81, 0xab, 0xdd, 0x19, 0x8c,
	0xba, 0x67, 0xb0, 0x8d, 0x1a, 0x5d, 0xfd, 0x6a, 0xc9, 0x8b, 0x5d, 0xfd, 0x6a, 0xcf, 0xc8, 0x84,
	0x0b, 0xa1, 0xc1, 0x98, 0x1b, 0x94, 0xf0, 0x91, 0x8d, 0xdd, 0xfe, 0x5b, 0xac, 0xe3, 0x5c, 0xb1,
	0xc9, 0x8e, 0x73, 0x45, 0x43, 0x32, 0xde, 0xd4, 0x0b, 0xb4, 0x6b, 0x76, 0xe0, 0x94, 0x26, 0xa2,
	0xef, 0x08, 0xd9, 0x94, 0x77, 0x58, 0xb2, 0xa9, 0x13, 0x1a, 0xa4, 0xbe, 0x8b, 0x27, 0x5d, 0x55,
	0xd2, 0xea, 0x35, 0x3b, 0xf4, 0x77, 0xd1, 0xa6, 0x75, 0x9f, 0x55, 0x8e, 0x16, 0x6e, 0x78, 0x02,
	0x4b, 0x76, 0x14, 0x06, 0xd1, 0x30, 0x6e, 0x10, 0x1a, 0x91, 0xc3, 0x52, 0xa3, 0xb4, 0x28, 0xb3,
	0x6f, 0x60, 0x73, 0x25, 0x0c, 0x3b, 0x0e, 0xfb, 0x51, 0x3f, 0xee, 0x62, 0x7a, 0x4e, 0x8e, 0xee,
	0x51, 0xa2, 0xc9, 0x51, 0x66, 0xdf, 0x4b, 0x8b, 0x4a, 0x1a, 0x46, 0x9d, 0xba, 0xc3, 0xe9, 0x05,
	0x39, 0x2e, 0xb5, 0x12, 0x55, 0x6a, 0x17, 0xdc, 0x42, 0xa6, 0x34, 0x82, 0x61, 0xaf, 0x9d, 0xbc,
	0x1b, 0xd4, 0x9d, 0x79, 0xfd, 0x86, 0x52, 0x5e, 0xcf, 0xbe, 0x55, 0x16, 0x0c, 0x3b, 0x71, 0xbb,
	0xd9, 0xe1, 0xf4, 0x94, 0x0c, 0x1f, 0x40, 0xe3, 0x3d, 0x82, 0x60, 0x6f, 0xdc, 0x6e, 0xb6, 0xf5,
	0xf5, 0xfe, 0x9d, 0x7f, 0x76, 0xc9, 0xc0, 0x0d, 0x9f, 0x1e, 0x03, 0x00, 0x00, 0xff, 0xff, 0x51,
	0x42, 0x8b, 0xf8, 0x9c, 0x03, 0x00, 0x00,
}
